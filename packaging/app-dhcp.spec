
Name: app-dhcp
Epoch: 1
Version: 2.5.28
Release: 1%{dist}
Summary: DHCP Server
License: GPLv3
Group: Applications/Apps
Packager: ClearFoundation
Vendor: ClearFoundation
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-network

%description
The DHCP Server app provides automatic IP configuration to devices on your network.

%package core
Summary: DHCP Server - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: app-network-core
Requires: app-events-core
Requires: dnsmasq >= 2.48

%description core
The DHCP Server app provides automatic IP configuration to devices on your network.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/dhcp
cp -r * %{buildroot}/usr/clearos/apps/dhcp/
rm -f %{buildroot}/usr/clearos/apps/dhcp/README.md

install -d -m 0755 %{buildroot}/var/clearos/dhcp
install -d -m 0755 %{buildroot}/var/clearos/dhcp/backup
install -D -m 0644 packaging/dnsmasq.logrotate %{buildroot}/etc/logrotate.d/dnsmasq
install -D -m 0755 packaging/network-configuration-event %{buildroot}/var/clearos/events/network_configuration/dhcp

%post
logger -p local6.notice -t installer 'app-dhcp - installing'

%post core
logger -p local6.notice -t installer 'app-dhcp-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/dhcp/deploy/install ] && /usr/clearos/apps/dhcp/deploy/install
fi

[ -x /usr/clearos/apps/dhcp/deploy/upgrade ] && /usr/clearos/apps/dhcp/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-dhcp - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-dhcp-api - uninstalling'
    [ -x /usr/clearos/apps/dhcp/deploy/uninstall ] && /usr/clearos/apps/dhcp/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/dhcp/controllers
/usr/clearos/apps/dhcp/htdocs
/usr/clearos/apps/dhcp/views

%files core
%defattr(-,root,root)
%doc README.md
%exclude /usr/clearos/apps/dhcp/packaging
%exclude /usr/clearos/apps/dhcp/unify.json
%dir /usr/clearos/apps/dhcp
%dir /var/clearos/dhcp
%dir /var/clearos/dhcp/backup
/usr/clearos/apps/dhcp/deploy
/usr/clearos/apps/dhcp/language
/usr/clearos/apps/dhcp/api
/usr/clearos/apps/dhcp/libraries
/etc/logrotate.d/dnsmasq
/var/clearos/events/network_configuration/dhcp
